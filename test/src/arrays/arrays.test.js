describe('Matchers Jest', () => {
	const user = {
		id: 1,
		username: 'Luis'
	}
	
	const names = ['Carlos', 'Luna', 'Karen', 'Pedro', 'Ariel'];

	function getNull() {
		return null;
	}
	

	function addName(name) {
		const result = existName(name);
		
		if(result) {
			throw new Error('El nombre ya existe');
		}
		
		return names.push(name);
	}

    function existName(name) {
        return names.some(n => n === name);
    }
	
	test('Deberían tener la misma referencia', () => {
		const userCopy = user;
		expect(userCopy).toBe(user);
	});
	
	test('Deberían ser iguales', () => {
		const user2 = {
			id: 1,
			username: 'Luis'
		};
		
		expect(user2).toEqual(user);
	});

	test('Debería tener longitud 6', () => {
		const newName = 'Fernando';
		
		names.push(newName);
		
		expect(names).toHaveLength(6);
	});

    test('Debería contener Ariel', () => {
		expect(names).toContain('Ariel');
	});

	test('Debería ser truthy al existir el nombre', () => {
		const result = existName('Carlos');
		
		expect(result).toBeTruthy();
	});
	
	test('Debería ser falsy al no existir el nombre', () => {
		const result = existName('No existe');
		
		expect(result).toBeFalsy();
	});
	
	test('Debería ser null', () => {
		const result = getNull();
		
		expect(result).toBeNull();
	});

	test('Debería lanzar una excepción si existe el nombre', () => {
		expect(() => addName('Carlos')).toThrow('El nombre ya existe');
	});
})